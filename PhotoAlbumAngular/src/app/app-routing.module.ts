import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddphotoComponent } from './addphoto/addphoto.component';
import { AuthGuard } from './helper/auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { MyphotoalbumComponent } from './myphotoalbum/myphotoalbum.component';
import { PhotoDetailsComponent } from './photo-details/photo-details.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },

  {
    path:'photo/:id',
    component:PhotoDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'addphoto',
    component:AddphotoComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'myalbum',
    component:MyphotoalbumComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'logout',
    component:LogoutComponent
  },
  {
    path:'register',
    component:RegisterComponent
  },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
