import { Component, DoCheck, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,DoCheck {
  form: FormGroup;
  isAuth:boolean;
  isError:boolean;

  constructor(private accountservice: AccountService, private router: Router) { 
    this.form = new FormGroup({
      'email': new FormControl('',Validators.required),
      'password': new FormControl('',Validators.required),
    });
    
  }
  
  
  ngOnInit(): void {
    this.isAuth=this.accountservice.isAuthorize();
    if(this.isAuth){
      this.router.navigate(['']);
    }
    
  }
  ngDoCheck(): void {
    
    this.isAuth=this.accountservice.isAuthorize();
    if(this.isAuth){
      this.router.navigate(['']);
    }

  }
  
  login(){
    this.accountservice.login(this.form.get('email')?.value,this.form.get('password')?.value);
    this.isError = true;
  }
  Close(){
    this.isError=false;
  }
}
