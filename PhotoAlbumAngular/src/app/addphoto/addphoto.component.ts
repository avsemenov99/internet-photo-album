import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
import { PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-addphoto',
  templateUrl: './addphoto.component.html',
  styleUrls: ['./addphoto.component.css']
})
export class AddphotoComponent implements OnInit {
  images:any;
    userEmail:string;
    userId:string;
    userRoles:string[];
   myForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', [Validators.required, Validators.minLength(3),Validators.maxLength(500)]),
   })

  constructor(private photoservice:PhotoService,private authservice:AccountService,private router:Router) {
    var decode = this.authservice.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
   }

  ngOnInit(): void {
  }
  get f(){
    return this.myForm.controls;
  }

  selectImage(event:any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.images = file;
    }
  }   

  submit(){
    

    console.log(this.myForm.value);

    this.photoservice.addphoto(this.myForm.get("name")?.value,this.myForm.get("description")?.value,this.images,this.userId)
      .subscribe(res => {
        console.log(res);
        alert('Uploaded Successfully.');
        this.router.navigate(['myalbum']);
      })
  }
}


