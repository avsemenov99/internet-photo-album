import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit,DoCheck {

  constructor(private accserv:AccountService, private router:Router) { }
  isAuth:boolean;

  ngOnInit(): void {
    
    this.isAuth=this.accserv.isAuthorize();
    if(this.isAuth==false){
      this.router.navigate(['login']);
    }
  }
  ngDoCheck(): void {
    this.isAuth=this.accserv.isAuthorize();
    if(this.isAuth==false){
      this.router.navigate(['login']);
    }
  }

  logout(){
    this.accserv.logout();
  }

}
