import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ErrorObserver, Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Registermodel } from '../models/registermode';

@Injectable({
  providedIn: 'root'
})



export class AccountService {

  private backend = "https://localhost:44348/api/Account/";

  constructor(private _http: HttpClient,private router:Router,private jwtHelper:JwtHelperService) { }

  login(email: string, password: string) {
    this._http.post(this.backend+'logon', {email: email,password: password},{responseType: 'text'}).subscribe(
     x=>localStorage.setItem('access_token',x));
    }

    register(reg:Registermodel){

      return this._http.post<Registermodel>(this.backend+'register',reg,{observe: 'response'})

    }
  
    isAuthorize(){
      if(localStorage.getItem('access_token')){
        return true;
      }
      else{
        return false;
      }
    }

    decode():[string,string,string[]]{
      var a = localStorage.getItem('access_token');
  
      if(a!=null){
  
        var tokenPayload = this.jwtHelper.decodeToken(a);
        var b = tokenPayload['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name']
        var c = tokenPayload['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier']
        var d = tokenPayload['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']
        return [b,c,d]
      }
      return ['','',[]]
    }
    
    logout(){
      if(localStorage.getItem('access_token')){
        localStorage.removeItem('access_token');
        return;
      }
      else{
        return;
      }
    }
  }

