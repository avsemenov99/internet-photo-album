import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rate } from '../models/rate';

@Injectable({
  providedIn: 'root'
})
export class RateService {

  private backend = "https://localhost:44348/api/Rate";

  constructor(private _http:HttpClient) { }

  addRate(rate:Rate){

    return this._http.post<Rate>(this.backend+'/',rate,{observe: 'response'})

  }
  updateRate(rate:Rate,userid:string){

    return this._http.put<Rate>(this.backend+"?userid="+userid,rate,{observe: 'response'})

  }

}
