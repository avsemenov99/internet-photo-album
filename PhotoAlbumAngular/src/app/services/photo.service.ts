import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Photo } from '../models/photo';


const form = new FormData();
@Injectable({
  providedIn: 'root'
})
export class PhotoService {

    private backend = "https://localhost:44348/api/Photo";

  constructor(private _http: HttpClient) { }

  getAllPhoto() : Observable<Photo[]>{
    return this._http.get<Photo[]>(this.backend);
  }
  addphoto(name:string,description:string,fileSource:any,userid:string){
    
    form.append('File', fileSource)
    form.append('Name', name)
    form.append('UserId', userid)
    form.append('Description', description)
    return this._http.post(`${this.backend}/`,form);
  }
  getalluserphoto(userid:string) : Observable<Photo[]>{
    return this._http.get<Photo[]>(`${this.backend}/user/${userid}`);
  }
  getMostPopular(numbers:number) : Observable<Photo[]>{
    return this._http.get<Photo[]>(`${this.backend}/MostPopular/${numbers}`);
  }
  getphoto(id:number): Observable<Photo>{
    return this._http.get<Photo>(`${this.backend}/${id}`);
  }
  deletephoto(id:number,userid:string){
    return this._http.delete(`${this.backend}/${id}?userid=${userid}`,{observe: 'response'});
  }
}
