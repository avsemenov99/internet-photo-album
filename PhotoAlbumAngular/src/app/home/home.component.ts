import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Photo } from '../models/photo';
import { PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  photolist: Photo[];
  allphoto: Photo[];
  isEnabled:boolean;
  searchfor:string;
  searchstring:string;
  constructor(private photoservice:PhotoService) {
    this.isEnabled = false;
   };

  ngOnInit(): void {
    this.photoservice.getMostPopular(50).subscribe(result=>this.photolist=result);
    
  }

  Close(){
    if(this.isEnabled){
      this.isEnabled = false
    }
    else{
      this.isEnabled=true
    }
    this.reset();
  }
  search(form:NgForm){
    this.searchstring = form.value['searchstring']
    this.searchfor = form.value['searchfor']
    if(!this.allphoto){
      this.allphoto=this.photolist;
    }
    if(this.searchfor == "for name" ){
      this.photolist = this.photolist.filter(x=>x.name.toLowerCase().includes(this.searchstring.toLowerCase()))
    }
    if(this.searchfor == "for description" ){
      this.photolist = this.photolist.filter(x=>x.description.toLowerCase().includes(this.searchstring.toLowerCase()))
    }
    if(!this.searchstring){
      this.reset();
    }
  }
  reset(){

    if(this.allphoto){
      this.photolist = this.allphoto;
    }
    
  }
}
