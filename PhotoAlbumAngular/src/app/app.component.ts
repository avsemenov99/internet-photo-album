import { Component, DoCheck, OnInit } from '@angular/core';
import { Photo } from './models/photo';
import { AccountService } from './services/account.service';
import { PhotoService } from './services/photo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,DoCheck {
  title = 'Internet Photo Album';
  isAuth:boolean;
  photolist: Photo[] = new Array<Photo>();
  constructor(private photoservice:PhotoService,private accountservice:AccountService){};
    
  ngOnInit(){
   this.isAuth=this.accountservice.isAuthorize();
   
  }
  ngDoCheck(): void {
    this.isAuth=this.accountservice.isAuthorize();
  }
}
