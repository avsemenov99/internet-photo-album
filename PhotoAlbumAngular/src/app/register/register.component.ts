import { Component, DoCheck, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Registermodel } from '../models/registermode';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit,DoCheck {
  isAuth:boolean;
  form:FormGroup;
  email:string;
  confirmedemail:string;
  password:string;
  confirmedpassword:string;
  firstname:string;
  lastname:string;
  isError:boolean;
  constructor(private authserv:AccountService,private router:Router) { 
    this.form = new FormGroup({
      'email': new FormControl('',Validators.required),
      'password': new FormControl('',Validators.required),
      'confirmedpassword': new FormControl('',Validators.required),
      'firstname': new FormControl('',Validators.required),
      'lastname': new FormControl('',Validators.required)
    })
  }
  reg: Registermodel;
  ngOnInit(): void {
    this.isAuth=this.authserv.isAuthorize();
    if(this.isAuth){
      this.router.navigate(['']);
    }
  }

  ngDoCheck(): void {
      this.isError;
  }
  register(){
    this.reg = new Registermodel(this.form.get('email')?.value,this.form.get('password')?.value,this.form.get('confirmedpassword')?.value,this.form.get('firstname')?.value,
    this.form.get('lastname')?.value)
    
    this.authserv.register(this.reg).subscribe({
      next: (v) => {
        if(v.status == 200){
          this.router.navigate(['login']);
        }
      },
      error: (e) => {
          this.isError=true;
      }
  })
}
  Close(){
    this.isError = false;
  }
  }


