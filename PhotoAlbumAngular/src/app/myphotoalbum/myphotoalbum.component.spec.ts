import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyphotoalbumComponent } from './myphotoalbum.component';

describe('MyphotoalbumComponent', () => {
  let component: MyphotoalbumComponent;
  let fixture: ComponentFixture<MyphotoalbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyphotoalbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyphotoalbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
