import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Photo } from '../models/photo';
import { AccountService } from '../services/account.service';
import { PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-myphotoalbum',
  templateUrl: './myphotoalbum.component.html',
  styleUrls: ['./myphotoalbum.component.css']
})
export class MyphotoalbumComponent implements OnInit,DoCheck {

  photolist: Photo[];
  isphoto:boolean;
  userid:string;
  email:string;
  roles:string[];

  constructor(private photoservice:PhotoService,private authservice:AccountService,private router:Router) {
    var decode = this.authservice.decode();
    this.email=decode[0];
    this.userid=decode[1];
    this.roles=decode[2];
   }

  ngOnInit(): void {
    this.photoservice.getalluserphoto(this.userid).subscribe(result=>this.photolist=result);
  }
  ngDoCheck(): void {
    if(this.photolist.length == 0){
      this.isphoto=false;
    }
    else{
      this.isphoto=true;
    }
  }

}
