export class Registermodel{
    email:string;
    password:string;
    passwordconfirm:string;
    firstname:string;
    lastname:string;

    constructor(email:string,password:string,passwordconf:string,firstname:string,lastname:string){
        this.email = email;
        this.password= password;
        this.passwordconfirm = passwordconf;
        this.firstname = firstname;
        this.lastname = lastname;
    }
}