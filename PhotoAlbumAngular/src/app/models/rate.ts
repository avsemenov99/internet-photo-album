export class Rate{
    id:number;
    photoId:number;
    userId:string;
    like:boolean;

    constructor(photoid:number, userid:string, like:boolean){
        this.photoId=photoid;
        this.userId=userid;
        this.like=like;
    }
}