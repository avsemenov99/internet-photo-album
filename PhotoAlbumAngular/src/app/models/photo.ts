import { Rate } from "./rate";


export class Photo {
    id: number;
    name: string;
    avgLikes: number;
    path: string;
    userId:string;
    description:string;
    tags:string[];
    rates:Rate[];
    email:string;
    createdDate?:Date;

    constructor(name:string,userid:string,description:string){

        this.name=name;
        this.userId=userid;
        this.description = description;

    }
}