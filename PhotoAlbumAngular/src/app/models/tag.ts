export class Tag{
    id:number;
    photoid:number;
    userid:string;
    nametag:string;

    constructor(id:number,photoid:number,userid:string,nametag:string){
        this.id=id;
        this.photoid=photoid;
        this.userid=userid;
        this.nametag=nametag;
    }
}