import { Component, DoCheck, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router, RouterLink, RouterLinkActive } from '@angular/router';
import { Subscription } from 'rxjs';
import { Photo } from '../models/photo';
import { Rate } from '../models/rate';
import { AccountService } from '../services/account.service';
import { PhotoService } from '../services/photo.service';
import { RateService } from '../services/rate.service';

@Component({
  selector: 'app-photo-details',
  templateUrl: './photo-details.component.html',
  styleUrls: ['./photo-details.component.css']
})
export class PhotoDetailsComponent implements OnInit,DoCheck,OnDestroy{

  @Input()
  sub:Subscription = new Subscription();
  photo:Photo;
  rate:Rate;
  userEmail:string;
  userId:string;
  userRoles:string[];
  like:boolean;
  liked:string = '/assets/liked.png';
  dontlike:string = '/assets/dontlike.png';
  constructor(private photoservice:PhotoService, private _route:ActivatedRoute,private accountservice:AccountService,private rateservise:RateService, private router: Router){
    var decode = this.accountservice.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
  }

  ngOnInit(): void {
    const id = this._route.snapshot.paramMap.get('id');
    this.sub.add(this.photoservice.getphoto(Number(id)).subscribe(photo=>this.photo=photo));
    if(!this.photo){
      return;
    }

    
  }
ngDoCheck(): void {
  if(this.photo && !this.rate){
    console.log(this.photo.rates)
    for(let element of this.photo.rates){
      if(element.userId == this.userId){
       this.rate= element;
        this.like=element.like;
        return;
      }
    }
  }
}

addRate(){
  
  if(!this.rate){
    this.like = true;
    this.rate = new Rate(this.photo.id, this.userId,this.like)
    this.sub.add(this.rateservise.addRate(this.rate).subscribe({
      next: (v) => {
        if(v.status == 200){
          this.ngOnInit()
        }
      },
      error: (e) => {
        alert(e);
      }
  }))
  }
  else{
    this.like=!this.like;
    this.rate.like=this.like;
    console.log(this.rate)
    this.sub.add(this.rateservise.updateRate(this.rate,this.userId).subscribe({
      next: (v) => {
        if(v.status == 200){
          this.ngOnInit()
        }
      },
      error: (e) => {
        console.log(e);
      }
  }))
  }
  }

  deletephoto(){
    this.sub.add(this.photoservice.deletephoto(this.photo.id,this.userId).subscribe({
      next: (v) => {
        if(v.status == 200){
          this.router.navigate(['']);
        }
      },
      error: (e) => {
        console.log(e);
      }
  }))
  }
  
ngOnDestroy(): void {
    this.sub.remove;
}
}
