﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbumPL.Filter
{
    public class ExceptionFilter : Attribute, IAsyncExceptionFilter
    {
        public async Task OnExceptionAsync(ExceptionContext context)
        {
            var action = context.ActionDescriptor.DisplayName;
            
            var exceptionMessage = context.Exception.Message;

            context.Result =new ContentResult
            {
                Content = $"[ExceptionFilter]Calling {action} failed, because: {exceptionMessage}.",
                StatusCode = 500
            };
            context.ExceptionHandled = true;
        }
    }
}
