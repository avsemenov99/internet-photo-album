﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PhotoAlbumBL.DTOModels;
using PhotoAlbumBL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbumPL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        private readonly IPhotoService _photoservice;
        private readonly ILogger<PhotoController> _logger;

        public PhotoController(IPhotoService photoservice, ILogger<PhotoController> logger)
        {
            _photoservice = photoservice;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PhotoDto>>> GetAll()
        {
            return Ok(await _photoservice.GetAllAsync());
        }

        [HttpGet("user/{userid}")]
        public async Task<ActionResult<IEnumerable<PhotoDto>>> GetPhotosByUser(string userid)
        {
            _logger.LogInformation($"Get Photos by userid={userid}");

            return Ok(await _photoservice.GetPhotoByUserIdAsync(userid));
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Add([FromForm] FileUpload a, [FromForm] AddPhotoDto photo)
        {
            _logger.LogInformation($"Added new photo photo name ={photo.Name} from user={photo.UserId}");
            return Ok(await _photoservice.AddAsync(photo, a));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> DeleteById(int id, string userid)
        {

            if (await _photoservice.DeleteByIdAsync(id, userid)) {
                _logger.LogInformation($"Remove photo photoid={id} from user={userid}");
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PhotoDto>> GetById(int id)
        {
            return Ok(await _photoservice.GetByIdAsync(id));
        }

        [HttpPut]
        [Authorize]
        public async Task<ActionResult> UpdatePhoto([FromBody] PhotoDto photo, string userid)
        {
            if (await _photoservice.UpdateAsync(photo, userid))
            {
                _logger.LogInformation($"Updated photo photoname ={photo.Name} from user={userid}");
                return Ok();
            }
            return BadRequest();
        }

        [Authorize]
        [HttpGet("MostPopular/{n}")]
        public async Task<ActionResult> GetMostPopular(int n)
        {
            return Ok(await _photoservice.GetMostPopularsPhotoAsync(n));
        }
    }
}
