﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PhotoAlbumBL.IdentityModels;
using PhotoAlbumBL.Interfaces;
using PhotoAlbumPL.JwtHelper;
using System.Threading.Tasks;

namespace PhotoAlbumPL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly JwtSettings _jwtSettings;
        private readonly ILogger<PhotoController> _logger;
        public AccountController(IUserService userService, IOptionsSnapshot<JwtSettings> jwtSettings,ILogger<PhotoController> logger)
        {
            _userService = userService;
            _jwtSettings = jwtSettings.Value;
            _logger = logger;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterDto model)
        {
            await _userService.Register(model);
            _logger.LogInformation($"{model.Email} Account was created");
            return Ok();
        }

        [HttpPost("logon")]
        public async Task<IActionResult> Logon(LogonDto model)
        {
            var user = await _userService.Logon(model);

            if (user is null) return BadRequest();
            _logger.LogInformation($"userEmail={model.Email} userid={user.Id} was authorizate");
            var roles = await _userService.GetRoles(user);
            return Ok(JwtHelper.JwtHelper.GenerateJwt(user, roles, _jwtSettings));

        }

        [HttpPost("createRole")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> CreateRole(RoleDto model)
        {
            await _userService.CreateRole(model.RoleName);
            _logger.LogInformation($"role={model.RoleName} was created");
            return Ok();
        }

        [HttpGet("getRoles")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetRoles()
        {
            _logger.LogInformation($"Get All Roles");
            return Ok(await _userService.GetRoles());
        }

        [HttpPost("AddRoleToUser")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> AddUserToRole(UsertoRoleDto model)
        {
            await _userService.AddRoleToUser(model);
            _logger.LogInformation($"userid={model.UserId} add roles {model.Roles}");
            return Ok();
        }


        [HttpPost("RemoveUserToRole")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> RemoveUserToRole(UsertoRoleDto model)
        {
            await _userService.RemoveUserRoles(model);
            _logger.LogInformation($"userid={model.UserId} remove roles {model.Roles}");
            return Ok();
        }
    }
}
