﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoAlbumBL.DTOModels;
using PhotoAlbumBL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbumPL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly ITagService _tagService;

        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagDto>>> GetAll()
        {
            return Ok(await _tagService.GetAllAsync());
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id,string userid)
        {
            if(await _tagService.DeleteByIdAsync(id,userid))
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TagDto>> GetById(int id)
        {
            return (await _tagService.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody]TagDto tag)
        {
            if(await _tagService.AddAsync(tag))
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<ActionResult> Update([FromBody]TagDto tag,string userid)
        {
            if(await _tagService.UpdateAsync(tag,userid))
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}
