﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoAlbumBL.DTOModels;
using PhotoAlbumBL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbumPL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RateController : ControllerBase
    {
        private readonly IRateService _rateService;

        public RateController(IRateService rateService)
        {
            _rateService = rateService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<RateDto>>> GetAll()
        {
            return Ok(await _rateService.GetAllAsync());
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> DeleteById(int id, string userid)
        {
            if(await _rateService.DeleteByIdAsync(id,userid))
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Add([FromBody]RateDto rate)
        {
            if(await _rateService.AddAsync(rate))
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<RateDto>> GetById(int id)
        {
            return Ok(await _rateService.GetByIdAsync(id));
        }


        [HttpPut]
        [Authorize]
        public async Task<ActionResult> Update([FromBody] RateDto rateDto,string userid)
        {
            if(await _rateService.UpdateAsync(rateDto,userid))
            {
                return Ok();
            }
            return BadRequest();
        }

    }
}
