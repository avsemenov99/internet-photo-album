﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PhotoAlbumBL.DTOModels;
using PhotoAlbumBL.Interfaces;
using PhotoAlbumDAL.Interfaces;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace PhotoAlbumBL.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly string[] _formats = { "jpg", "jpeg", "png", "bmp", "jfif" };
        private const string path = "D:\\Final Proj\\PhotoAlbumAngular\\src\\assets\\Uploads\\";

        public PhotoService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }
        public async Task<bool> AddAsync(AddPhotoDto photo,FileUpload file)
        {
            
            var user = _userManager.FindByIdAsync(photo.UserId);
            if (user == null)
            {
                throw new ArgumentException("User dont found");
            }
            string fileformat = System.IO.Path.GetExtension(file.File.FileName).Substring(1);

            if (!_formats.Any(x => x.Contains(fileformat))){
                throw new ArgumentException("incorect file extenshion(Need jpg,bmp,jpeg,jfif or png)");
            
            }
            string photopath = user.Result.FirstName  +user.Result.LastName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "." + fileformat;

            //string path = Directory.GetParent(Directory.GetCurrentDirectory()).FullName + "\\Uploads\\";

            if (string.IsNullOrEmpty(photo.Name) || string.IsNullOrEmpty(photo.Description))
            {
                throw new ArgumentException("Model dont have Name or Description");
            }

            try
            { 
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                using (FileStream fileStream = System.IO.File.Create(path + photopath))
                {
                    file.File.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            photopath = "/assets/Uploads/" + photopath;
            var newphoto = _mapper.Map<Photo>(photo);
            newphoto.Path = photopath;
            newphoto.CreatedDate = DateTime.Now;
            newphoto.AvgLikes = 0;
            newphoto.Email = user.Result.Email;
            await _unitOfWork.PhotoRepository.AddAsync(newphoto);
            await _unitOfWork.SaveAsync();
            return true;

        }

        public async Task<bool> DeleteByIdAsync(int id,string userid)
        {
            var user = await _userManager.FindByIdAsync(userid);
            if (user == null)
            {
                throw new ArgumentException("User dont found");
            }

            var roles = await _userManager.GetRolesAsync(user); 
            Photo model = await _unitOfWork.PhotoRepository.GetByIdAsync(id);

            if(model.UserId == userid || roles.Contains("admin"))
            {
                bool b = await _unitOfWork.PhotoRepository.DeleteByIdAsync(id);
                await _unitOfWork.SaveAsync();
                return b;
            }
            
            else
            {
                throw new ArgumentException("you cannot delete a photo that is not yours");
            }
        }

        public async Task<IEnumerable<PhotoDto>> GetAllAsync()
        {
            var photos = await _unitOfWork.PhotoRepository.GetAllWithDetails().ToListAsync();
            var photosdto = _mapper.Map<IEnumerable<PhotoDto>>(photos);
            foreach(var item in photosdto)
            {

                item.Tags = photos.FirstOrDefault(x => x.Id == item.Id).Tags.Select(x=>x.NameTag);
            }
            return photosdto;
        }

        public async Task<PhotoDto> GetByIdAsync(int id)
        {
            
            var a = await _unitOfWork.PhotoRepository.GetByIdAsync(id);
            if (a != null)
            {
                PhotoDto photodto = _mapper.Map<PhotoDto>(a);
                photodto.Tags = a.Tags.Select(x => x.NameTag);
               // photodto.Rates = _mapper.Map<IEnumerable<RateDto>>(a.Rates);
                return photodto;
            }
            else
            {
                throw new ArgumentException($"Photo with id-{id} not found");
            }

        }

        public async Task<IEnumerable<PhotoDto>> GetPhotoByUserIdAsync(string userid)
        {
            var user = await _userManager.FindByIdAsync(userid);
            if(user == null)
            {
                throw new ArgumentException("User not found");
            }
            var photos = await _unitOfWork.PhotoRepository.GetAllWithDetails().Where(x=>x.UserId == userid).ToListAsync();
            if (photos != null)
            {
                var photosdto = _mapper.Map<IEnumerable<PhotoDto>>(photos);

                foreach (var item in photosdto)
                {

                    item.Tags = photos.FirstOrDefault(x => x.Id == item.Id).Tags.Select(x => x.NameTag);
                }
                return photosdto;
            }
            else
            {
                throw new ArgumentException($"Photos not found");
            }
        }

        public async Task<bool> UpdateAsync(PhotoDto photo,string userid)
        {

            var user = await _userManager.FindByIdAsync(photo.UserId);
            if(user == null)
            {
                throw new ArgumentException("User dont found");
            }
            var roles = await _userManager.GetRolesAsync(user);
            if (photo.UserId == userid || roles.Contains("admin"))
            {
                return await _unitOfWork.PhotoRepository.UpdateAsync(_mapper.Map<Photo>(photo));
            }
            else
            {
                throw new ArgumentException("you cannot update a photo that is not yours");
            }
        }
        public async Task<IEnumerable<PhotoDto>> GetMostPopularsPhotoAsync(int n)
        {
            var photos = await _unitOfWork.PhotoRepository.GetAllWithDetails().OrderByDescending(x => x.AvgLikes).ThenBy(x=>x.Name).Take(n).ToListAsync();
            if (photos != null)
            {
                var photosdto = _mapper.Map<IEnumerable<PhotoDto>>(photos);

                foreach (var item in photosdto)
                {

                    item.Tags = photos.FirstOrDefault(x => x.Id == item.Id).Tags.Select(x => x.NameTag);
                }
                return photosdto;
            }
            else
            {
                throw new ArgumentException($"Photos not found");
            }
        }
    }
}
