﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoAlbumBL.DTOModels;
using PhotoAlbumBL.Interfaces;
using PhotoAlbumDAL.Interfaces;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumBL.Services
{

    public class RateService : IRateService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public RateService(IUnitOfWork unitOfWork, IMapper mapper,UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }
        public async Task<bool> AddAsync(RateDto rate)
        {
            var user = await _userManager.FindByIdAsync(rate.UserId);
            if (user == null)
            {
                throw new ArgumentException("User not found");
            }
            var item = _unitOfWork.RateRepository.GetAll().Where(x => x.PhotoId == rate.PhotoId && x.Like == rate.Like && x.UserId == rate.UserId).ToList();
            var photo = await _unitOfWork.PhotoRepository.GetByIdAsync(rate.PhotoId);
            if (item.Count==0 && photo !=null)
            {
                var a = _mapper.Map<Rate>(rate);
                bool b = await _unitOfWork.RateRepository.AddAsync(a);
                if (b)
                {
                    photo.AvgLikes++;
                }
                await _unitOfWork.PhotoRepository.UpdateAsync(photo);
                await _unitOfWork.SaveAsync();
                return b;
            }
            else
            {
                throw new ArgumentException("You cant like or dislike this photo 2 times");
            }
        }

        public async Task<bool> DeleteByIdAsync(int id,string userid)
        {
            var user = await _userManager.FindByIdAsync(userid);
            if (user == null)
            {
                throw new ArgumentException("User not found");
            }
            var roles = await _userManager.GetRolesAsync(user);
            Rate rate = await _unitOfWork.RateRepository.GetByIdAsync(id);
            if (rate == null)
            {
                throw new ArgumentException($"{id}-rate.id dont exist");
            }
            if (rate.UserId == userid || roles.Contains("admin"))
            {
                
                bool b = await _unitOfWork.RateRepository.DeleteByIdAsync(id);
                if (b)
                {
                    var photo = await _unitOfWork.PhotoRepository.GetByIdAsync(rate.PhotoId);
                    photo.AvgLikes--;
                    await _unitOfWork.PhotoRepository.UpdateAsync(photo);
                }
                await _unitOfWork.SaveAsync();
                return b;
            }
            else
            {
                throw new ArgumentException("You cannot delete a rate that is not yours");
            }
                
            
            
        }

        public async Task<IEnumerable<RateDto>> GetAllAsync()
        {
            var a = await _unitOfWork.RateRepository.GetAll().ToListAsync();

            return _mapper.Map<IEnumerable<RateDto>>(a);
        }

        public async Task<RateDto> GetByIdAsync(int id)
        {
            var a = await _unitOfWork.RateRepository.GetByIdAsync(id);
            if (a != null)
            {
                return _mapper.Map<RateDto>(a);
            }
            else
            {
                throw new ArgumentException($"rate with id={id} is not found");
            }

        }

 
        public async Task<bool> UpdateAsync(RateDto rate, string userid)
        {

            var user = await _userManager.FindByIdAsync(rate.UserId);
            var roles = await _userManager.GetRolesAsync(user);
            if (rate.UserId == userid || roles.Contains("admin"))
            {
                var ratebd = await _unitOfWork.RateRepository.GetByIdAsync(rate.Id);
                if (ratebd == null)
                {
                    throw new ArgumentException("rate dont found");
                }
                bool res = await _unitOfWork.RateRepository.UpdateAsync(_mapper.Map<Rate>(rate));
                if (res)
                {
                    var photo = await _unitOfWork.PhotoRepository.GetByIdAsync(rate.PhotoId);
                    if (rate.Like)
                    {
                        photo.AvgLikes++;
                    }
                    else
                    {
                        photo.AvgLikes--;
                    }
                    
                    await _unitOfWork.PhotoRepository.UpdateAsync(photo);
                }
                await _unitOfWork.SaveAsync();
                return res;
            }
            else
            {
                throw new ArgumentException("You cant update rate, that is not yours");
            }
        }
    }
}

