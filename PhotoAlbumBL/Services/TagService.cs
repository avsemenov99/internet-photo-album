﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoAlbumBL.DTOModels;
using PhotoAlbumBL.Interfaces;
using PhotoAlbumDAL.Interfaces;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumBL.Services
{
    public class TagService:ITagService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _usermanager;

        public TagService(IUnitOfWork unitOfWork, IMapper mapper,UserManager<ApplicationUser> usermanager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _usermanager = usermanager;
        }
        public async Task<bool> AddAsync(TagDto tag)
        {

            var photo = await _unitOfWork.PhotoRepository.GetByIdAsync(tag.PhotoId);
            var user = _usermanager.FindByIdAsync(tag.UserId);
            if(user == null)
            {
                throw new ArgumentException("User not found");
            }
            if (photo !=null)
            {
                var a = _mapper.Map<Tag>(tag);
                bool b = await _unitOfWork.TagRepository.AddAsync(a);
                await _unitOfWork.SaveAsync();
                return b;
            }
            else
            {
                throw new ArgumentException($"Photo id - {tag.UserId} dont found");
            }
            
        }

        public async Task<bool> DeleteByIdAsync(int id,string userid)
        {
            var user = await _usermanager.FindByIdAsync(userid);
            if (user == null)
            {
                throw new ArgumentException("User with this userid dont found");
            }
            var roles = await _usermanager.GetRolesAsync(user);
            Tag tag = await _unitOfWork.TagRepository.GetByIdAsync(id);
            if (tag == null)
            {
                throw new ArgumentException($"{id}-rate.id dont exist");
            }
            if (tag.UserId == userid || roles.Contains("admin"))
            {
                bool b = await _unitOfWork.TagRepository.DeleteByIdAsync(id);
                await _unitOfWork.SaveAsync();
                return b;
            }
            else
            {
                throw new ArgumentException("You cannot delete tag in this photo, that is not yours");
            }
                
        }

        public async Task<IEnumerable<TagDto>> GetAllAsync()
        {
            var a = await _unitOfWork.TagRepository.GetAllWithDetails().ToListAsync();

            return _mapper.Map<IEnumerable<TagDto>>(a);
        }

        public async Task<TagDto> GetByIdAsync(int id)
        {
            var a = await _unitOfWork.TagRepository.GetByIdAsync(id);
            if (a == null)
            {
                throw new ArgumentException($"Tag with id-{id} not Found");
            }
            return _mapper.Map<TagDto>(a);

        }

        public async Task<bool> UpdateAsync(TagDto tag,string userid)
        {
            var user = await _usermanager.FindByIdAsync(tag.UserId);
            var roles = await _usermanager.GetRolesAsync(user);
            if (tag.UserId == userid || roles.Contains("admin"))
            {
                return await _unitOfWork.TagRepository.UpdateAsync(_mapper.Map<Tag>(tag));
            }
            else
            {
                throw new ArgumentException("You cant update this tag, that is not yours");
            }

                
        }
    }
}

