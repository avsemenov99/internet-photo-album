﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PhotoAlbumBL.IdentityModels;
using PhotoAlbumBL.Interfaces;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumBL.Services
{
    public class UserService:IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserService(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Register(RegisterDto user)
        {
            var usr = new ApplicationUser
            {
                Email = user.Email,
                UserName = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            var result = await _userManager.CreateAsync(usr, user.Password);

            if (!result.Succeeded)
            {
                throw new ArgumentException(string.Join(';', result.Errors.Select(x => x.Description)));
            }

            await _userManager.AddToRoleAsync(usr, "user");
        }

        public async Task<ApplicationUser> Logon(LogonDto logon)
        {
            var user = await _userManager.Users.SingleOrDefaultAsync(u => u.UserName == logon.Email);
            if (user is null) throw new ArgumentException($"User not found: '{logon.Email}'.");

            return await _userManager.CheckPasswordAsync(user, logon.Password) ? user : null;
        }


        public async Task CreateRole(string roleName)
        {
            var result = await _roleManager.CreateAsync(new IdentityRole(roleName));

            if (!result.Succeeded)
            {
                throw new ArgumentException($"Role could not be created: {roleName}.");
            }
        }

        public async Task AddRoleToUser(UsertoRoleDto usertorole)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Id == usertorole.UserId);

            if (user == null)
            {
                throw new ArgumentException("Error id");
            }

            var roles = _roleManager.Roles.AsEnumerable().Where(r => usertorole.Roles.Contains(r.Name, StringComparer.OrdinalIgnoreCase))
                .Select(r => r.NormalizedName).ToList();

            if (roles.Count == 0)
            {
                throw new ArgumentException("Error role");
            }

            var result = await _userManager.AddToRolesAsync(user, roles);

            if (!result.Succeeded)
            {
                throw new ArgumentException(string.Join(';', result.Errors.Select(x => x.Description)));
            }
        }

        public async Task<IEnumerable<IdentityRole>> GetRoles()
        {
            return await _roleManager.Roles.ToListAsync();
        }

        public async Task<IEnumerable<string>> GetRoles(ApplicationUser user)
        {
            return (await _userManager.GetRolesAsync(user)).ToList();
        }

        public async Task RemoveUserRoles(UsertoRoleDto userToRole)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Id == userToRole.UserId);

            if (user == null)
            {
                throw new ArgumentException("Error id");
            }

            var roles = _roleManager.Roles.AsEnumerable().Where(r => userToRole.Roles.Contains(r.Name, StringComparer.OrdinalIgnoreCase))
                .Select(r => r.NormalizedName).ToList();

            if (roles.Count == 0)
            {
                throw new ArgumentException("Error role");
            }

            var result = await _userManager.RemoveFromRolesAsync(user, roles);

            if (!result.Succeeded)
            {
                throw new ArgumentException(string.Join(';', result.Errors.Select(x => x.Description)));
            }
        }
    }
}
