﻿using AutoMapper;
using PhotoAlbumBL.DTOModels;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoAlbumBL
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Photo, PhotoDto>().ReverseMap();
            CreateMap<Rate, RateDto>().ReverseMap();
            CreateMap<Tag, TagDto>().ReverseMap();
            CreateMap<Photo, AddPhotoDto>().ReverseMap();
        }
    }
}
