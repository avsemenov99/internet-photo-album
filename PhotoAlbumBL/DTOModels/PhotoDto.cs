﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoAlbumBL.DTOModels
{
    public class PhotoDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public int? AvgLikes { get; private set; }
        public string Path { get; private set; }
        [Required(ErrorMessage = "UserId is required")]
        public string UserId { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        public string Email { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IEnumerable<RateDto> Rates { get; set; }
        public DateTime CreatedDate { get; private set; }

    }
}
