﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoAlbumBL.DTOModels
{
    public class TagDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "PhotoId is required")]
        public int PhotoId { get; set; }
        [Required(ErrorMessage = "UserId is required")]
        public string UserId { get; set; }
        [Required(ErrorMessage = "NameTag"), MaxLength(20)]
        public string NameTag { get; set; }

    }
}
