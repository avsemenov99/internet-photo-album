﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoAlbumBL.DTOModels
{
    public class FileUpload
    {
        [Required]
        public IFormFile File { get; set; }
    }
}
