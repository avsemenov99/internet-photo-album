﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoAlbumBL.IdentityModels
{
    public class RoleDto
    {
        [Required, MinLength(1), MaxLength(20)]
        public string RoleName { get; set; }
    }
}
