﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoAlbumBL.IdentityModels
{
    public class UsertoRoleDto
    {

        [Required]
        public string UserId { get; set; }
        [Required, MinLength(1)]
        public string[] Roles { get; set; }

    }
}
