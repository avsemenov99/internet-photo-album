﻿using Microsoft.AspNetCore.Http;
using PhotoAlbumBL.DTOModels;
using PhotoAlbumDAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoAlbumBL.Interfaces
{
    public interface IPhotoService
    {
        Task<bool> AddAsync(AddPhotoDto photo,FileUpload file);
        Task<bool> UpdateAsync(PhotoDto photo,string userid);
        Task<IEnumerable<PhotoDto>> GetPhotoByUserIdAsync(string userid);
        Task<IEnumerable<PhotoDto>> GetAllAsync();
        Task<PhotoDto> GetByIdAsync(int id);
        Task<bool> DeleteByIdAsync(int id,string userid);
        Task<IEnumerable<PhotoDto>> GetMostPopularsPhotoAsync(int n);
    }
}
