﻿using PhotoAlbumBL.DTOModels;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumBL.Interfaces
{
    public interface ITagService
    {
        Task<bool> AddAsync(TagDto tag);
        Task<bool> UpdateAsync(TagDto tag,string userid);
        Task<IEnumerable<TagDto>> GetAllAsync();
        Task<TagDto> GetByIdAsync(int id);
        Task<bool> DeleteByIdAsync(int id,string userid);
    }
}
