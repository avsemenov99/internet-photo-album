﻿using PhotoAlbumBL.DTOModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumBL.Interfaces
{
    public interface IRateService
    {
        Task<bool> AddAsync(RateDto rate);
        Task<bool> UpdateAsync(RateDto rate,string userid);
        Task<IEnumerable<RateDto>> GetAllAsync();
        Task<RateDto> GetByIdAsync(int id);
        Task<bool> DeleteByIdAsync(int id,string userid);
    }
}
