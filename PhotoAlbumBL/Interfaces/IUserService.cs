﻿using Microsoft.AspNetCore.Identity;
using PhotoAlbumBL.IdentityModels;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumBL.Interfaces
{
    public interface IUserService
    {
        Task Register(RegisterDto user);
        Task<ApplicationUser> Logon(LogonDto logon);
        Task CreateRole(string roleName);
        Task AddRoleToUser(UsertoRoleDto userToRole);
        Task RemoveUserRoles(UsertoRoleDto userToRole);
        Task<IEnumerable<string>> GetRoles(ApplicationUser user);
        Task<IEnumerable<IdentityRole>> GetRoles();
    }
}
