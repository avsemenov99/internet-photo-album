﻿using PhotoAlbumDAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbumDAL.Interfaces
{
    public interface IRateRepository
    {
        IQueryable<Rate> GetAll();
        IQueryable<Rate> GetAllWithDetails();
        Task<Rate> GetByIdAsync(int id);
        Task<Rate> GetByIdWithDetailsAsync(int id);
        Task<bool> AddAsync(Rate rate);

        Task<bool> UpdateAsync(Rate rate);

        Task<bool> DeleteAsync(Rate rate);

        Task<bool> DeleteByIdAsync(int id);
    }
}
