﻿using PhotoAlbumDAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbumDAL.Interfaces
{
    public interface IPhotoRepository
    {
        IQueryable<Photo> GetAll();
        IQueryable<Photo> GetAllWithDetails();
        Task<Photo> GetByIdAsync(int id);
        Task<bool> AddAsync(Photo photo);

        Task<bool> UpdateAsync(Photo photo);

        Task<bool> DeleteAsync(Photo photo);

        Task<bool> DeleteByIdAsync(int id);
        Task<Photo> GetByIdWithDetailsAsync(int id);
    }
}
