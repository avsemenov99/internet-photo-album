﻿using PhotoAlbumDAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbumDAL.Interfaces
{
    public interface ITagRepository
    {
        IQueryable<Tag> GetAll();
        IQueryable<Tag> GetAllWithDetails();
        Task<Tag> GetByIdAsync(int id);
        Task<Tag> GetByIdWithDetailsAsync(int id);
        Task<bool> AddAsync(Tag tag);

        Task<bool> UpdateAsync(Tag tag);

        Task<bool> DeleteAsync(Tag tag);

        Task<bool> DeleteByIdAsync(int id);
    }
}
