﻿using System.Threading.Tasks;

namespace PhotoAlbumDAL.Interfaces
{
    public interface IUnitOfWork
    {
        IPhotoRepository PhotoRepository { get; }
        ITagRepository TagRepository { get; }
        IRateRepository RateRepository { get; }
        Task<int> SaveAsync();
    }
}
