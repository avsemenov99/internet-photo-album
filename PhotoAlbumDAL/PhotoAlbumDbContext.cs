﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoAlbumDAL
{
    public class PhotoAlbumDbContext:IdentityDbContext<ApplicationUser>
    {
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public PhotoAlbumDbContext(DbContextOptions<PhotoAlbumDbContext> options) : base(options)
        {
            
        }
        
    }
}
