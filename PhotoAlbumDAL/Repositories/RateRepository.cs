﻿using Microsoft.EntityFrameworkCore;
using PhotoAlbumDAL.Interfaces;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumDAL.Repositories
{
    public class RateRepository:IRateRepository
    {
        private readonly PhotoAlbumDbContext _context;

        public RateRepository(PhotoAlbumDbContext context)
        {
            _context = context;
        }
        public async Task<bool> AddAsync(Rate rate)
        {
            await _context.Rates.AddAsync(rate);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(Rate rate)
        {
            var item = await _context.Rates.FirstOrDefaultAsync(x => x.Id == rate.Id);

            if (item != null)
            {
                _context.Rates.Remove(rate);
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            var item = await _context.Rates.FirstOrDefaultAsync(x => x.Id == id);

            if (item != null)
            {
                _context.Rates.Remove(item);

                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public IQueryable<Rate> GetAll()
        {
            return _context.Rates;
        }

        public IQueryable<Rate> GetAllWithDetails()
        {
            return _context.Rates.Include(x => x.Photo);
        }

        public async Task<Rate> GetByIdAsync(int id)
        {
            return await _context.Rates.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Rate> GetByIdWithDetailsAsync(int id)
        {
            return await _context.Rates.Include(x => x.Photo).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> UpdateAsync(Rate rate)
        {
            Rate item = await GetByIdAsync(rate.Id);
            item.Like = rate.Like;
            item.PhotoId = rate.PhotoId;
            await _context.SaveChangesAsync();
            return item != null;
        }
    }
}
