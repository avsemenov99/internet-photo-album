﻿using PhotoAlbumDAL.Interfaces;
using System.Threading.Tasks;
using PhotoAlbumDAL.Models;
using System.Linq;
using PhotoAlbumDAL;
using Microsoft.EntityFrameworkCore;

namespace PhotoAlbumDAL.Repositories
{
    public class TagRepository:ITagRepository
    {
        private readonly PhotoAlbumDbContext _context;

        public TagRepository(PhotoAlbumDbContext context)
        {
            _context = context;
        }
        public async Task<bool> AddAsync(Tag tag)
        {
            await _context.Tags.AddAsync(tag);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(Tag tag)
        {
            var item = await _context.Tags.FirstOrDefaultAsync(x => x.Id == tag.Id);

            if (item != null)
            {
                _context.Tags.Remove(tag);
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            var item = await _context.Tags.FirstOrDefaultAsync(x => x.Id == id);

            if (item != null)
            {
                _context.Tags.Remove(item);

                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public IQueryable<Tag> GetAll()
        {
            return _context.Tags;
        }

        public IQueryable<Tag> GetAllWithDetails()
        {
            return _context.Tags.Include(x => x.Photo);
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            return await _context.Tags.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Tag> GetByIdWithDetailsAsync(int id)
        {
            return await _context.Tags.Include(x=>x.Photo).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> UpdateAsync(Tag tag)
        {
            Tag item = await GetByIdAsync(tag.Id);
            item.NameTag = tag.NameTag;
            item.PhotoId = tag.PhotoId;
            await _context.SaveChangesAsync();
            return item != null;
        }

    }
}
