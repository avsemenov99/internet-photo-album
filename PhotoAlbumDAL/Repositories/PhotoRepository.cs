﻿using Microsoft.EntityFrameworkCore;
using PhotoAlbumDAL.Interfaces;
using PhotoAlbumDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumDAL.Repositories
{
    public class PhotoRepository : IPhotoRepository
    {
        private readonly PhotoAlbumDbContext _context;

        public PhotoRepository(PhotoAlbumDbContext context)
        {
            _context = context;
        }

        public async Task<bool> AddAsync(Photo photo)
        {
            await _context.Photos.AddAsync(photo);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(Photo photo)
        {
            var item = await _context.Photos.FirstOrDefaultAsync(x => x.Id == photo.Id);

            if (item != null)
            {
                _context.Photos.Remove(photo);
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            var item = await _context.Photos.FirstOrDefaultAsync(x => x.Id == id);

            if (item != null)
            {
                _context.Photos.Remove(item);

                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public IQueryable<Photo> GetAll()
        {
            return _context.Photos;
        }

        public IQueryable<Photo> GetAllWithDetails()
        {
            return _context.Photos.Include(x => x.Rates).Include(x => x.Tags);
        }

        public async Task<Photo> GetByIdAsync(int id)
        {
            return await _context.Photos.Include(x=>x.Tags).Include(x=>x.Rates).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Photo> GetByIdWithDetailsAsync(int id)
        {
            return await _context.Photos.Include(x=>x.Rates).Include(x=>x.Tags).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> UpdateAsync(Photo photo)
        {
            Photo item = await GetByIdAsync(photo.Id);
            item.Name = photo.Name;
            item.Path = photo.Path;
            item.Description = photo.Description;
            item.CreatedDate = photo.CreatedDate;
            item.AvgLikes = photo.AvgLikes;
            item.Email = photo.Email;
            await _context.SaveChangesAsync();
            return item != null;    
        }
    }
}
