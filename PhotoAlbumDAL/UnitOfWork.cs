﻿using PhotoAlbumDAL.Interfaces;
using PhotoAlbumDAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotoAlbumDAL
{
    public class UnitOfWork:IUnitOfWork
    {
        private readonly PhotoAlbumDbContext _context;
        private PhotoRepository photoRepository;
        private TagRepository tagRepository;
        private RateRepository rateRepository;
        

        public UnitOfWork(PhotoAlbumDbContext context)
        {
            _context = context;
        }

        public IPhotoRepository PhotoRepository
        {
            get
            {
                if (photoRepository == null)
                    photoRepository = new PhotoRepository(_context);
                return photoRepository;
            }
        }

        public ITagRepository TagRepository
        {
            get
            {
                if (tagRepository == null)
                    tagRepository = new TagRepository(_context);
                return tagRepository;
            }
        }

        public IRateRepository RateRepository
        {
            get
            {
                if (rateRepository == null)
                    rateRepository = new RateRepository(_context);
                return rateRepository;
            }
        }


        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
