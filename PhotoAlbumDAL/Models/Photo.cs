﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoAlbumDAL.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int AvgLikes { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }

    }
}
