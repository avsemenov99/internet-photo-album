﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoAlbumDAL.Models
{
    public class Rate
    {
        public int Id { get; set; }
        public int PhotoId { get; set; }
        public bool Like { get; set; }
        public string UserId { get; set; }
        public virtual Photo Photo { get; set; }

    }
}
