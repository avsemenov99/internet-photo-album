﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotoAlbumDAL.Migrations
{
    public partial class avgrate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "AvgRate",
                table: "Photos",
                type: "real",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AvgRate",
                table: "Photos");
        }
    }
}
